<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// <--- Note --->
//membuat route dengan params1 (url) params2 (name of controller)
Route::get('/mentors', 'MentorController@index'); //mengambil seluruh data mentor
Route::get('/mentors/{id}', 'MentorController@show'); //mengambil detail data mentor
Route::post('/mentors', 'MentorController@create'); //membuat data mentor
Route::put('/mentors/{id}', 'MentorController@update');  //mengupdate data mentor
Route::delete('/mentors/{id}', 'MentorController@destroy'); // menghapus data mentor

Route::get('/courses', 'CourseController@index'); //get data courses
Route::post('/courses', 'CourseController@create'); //membuat data courses
Route::get('/courses/{id}', 'CourseController@show'); //menampilan data courses deengan id
Route::put('/courses/{id}', 'CourseController@update'); //mengupdate data courses
Route::delete('/courses/{id}', 'CourseController@destroy'); //men delete data courses

Route::get('/chapters', 'ChapterController@index'); //mengambil data chapter
Route::post('/chapters', 'ChapterController@create'); //membuat data chapter
Route::put('/chapters/{id}', 'ChapterController@update'); //meng update data chapter
Route::get('/chapters/{id}', 'ChapterController@show'); //meng ngambil detail data chapter sesuai id
Route::delete('/chapters/{id}', 'ChapterController@destroy'); //meng hapus data chapter 

Route::get('/lessons', 'LessonController@index'); //mengambil data lesson
Route::post('/lessons', 'LessonController@create'); //mengambil data lesson
Route::put('/lessons/{id}', 'LessonController@update'); //mengupdate data lesson
Route::get('/lessons/{id}', 'LessonController@show'); //meng ngambil detail data chapter sesuai id
Route::delete('/lessons/{id}', 'LessonController@destroy'); //meng hapus data lesson

Route::post('/image-courses', 'ImageCourseController@create'); //membuat data image-courses
Route::delete('/image-courses/{id}', 'ImageCourseController@destroy'); //meng hapus data image-courses

Route::post('/my-courses', 'MyCourseController@create'); //membuat data my-courses
Route::get('/my-courses', 'MyCourseController@index'); //mengambil data my-courses

Route::post('/reviews', 'ReviewController@create'); //membuat data review
Route::put('/reviews/{id}', 'ReviewController@update'); //membuat update data review
Route::delete('/reviews/{id}', 'ReviewController@destroy'); //delete data review
