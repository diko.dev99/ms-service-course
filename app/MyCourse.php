<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyCourse extends Model
{
    protected $table = 'my_courses'; //mengarahkan ke table my_courses

    protected $fillable = [ //mendeskripsikan kolom apa saja yg bisa digunakan / diisi
        'course_id', 'user_id'
    ];

    // variabel untuk merubah format tanggal created_at dan updated_at
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:m:s',
        'updated_at' => 'datetime:Y-m-d H:m:s'
    ];

    // membuat method course untuk mengambil data courses
    public function course()
    {
        return $this->belongsTo('App\Course');
    }

}
