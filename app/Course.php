<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses'; //mengarahkan ke table courses

    protected $fillable = [ //mendeskripsikan kolom apa saja yg boleh diisi
        'name', 'certificate', 'thumbnail', 'type',
        'status', 'price', 'level', 'description', 'mentor_id'
    ];

    // variabel untuk merubah format tanggal created_at dan updated_at
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:m:s',
        'updated_at' => 'datetime:Y-m-d H:m:s'
    ];

    // method untuk mendapatkan model lain, dan harus ada foreignKey yg menghubungkan ke 2 model tsb
    public function mentors()
    {
        return $this->belongsTo('App\Mentor'); //memdapatkan data mentor dari foreignKey mentor_id
    }
    public function chapters()
    {
        return $this->hasMany('App\Chapter')->orderBy('id', 'ASC'); //orderBY yaitu mengurutkan berdasarkan id secara ASC(ascending)
    }
    public function images()
    {
        return $this->hasMany('App\ImageCourse')->orderBy('id', 'DESC'); //orderBY yaitu mengurutkan berdasarkan id secara DESC(Descending)
    }
}
