## Pembuatan model
Model merupakan salah satu dari bagian MVC yang akan berkomunikasi dengan database.
# php artisan make:model

-Chapter
-Mycourse
-Mentor
-Review
-Course
-ImageCourse
-MyCourse
-Lesson

## Helper
file ini berisikan function-function yg bisa dipakai dimana saja, dimana helper ini akan
membantu integrasi service-user dan service-course

# step-by-step
-buat file di app dengan nama helpers.php , app/helpers.php
-kemudian daftarkan file helpers.php di file composer.json letakan didalam key autoload
-setelah selesai didaftarkan,
-stop server dan running ulang dengan perintah #composer dump-autoload hal ini untuk mereload kembali settingan
yg ada dicomposer.json

# ex: 

"files": [
   "app/helpers.php" --> sesuai nama dan path helpernya
]
