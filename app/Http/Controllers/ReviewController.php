<?php

namespace App\Http\Controllers;

use App\Course; //memanggil model course dari folder App
use App\Review; //memanggil model Mentor dari folder App
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; //mengimport validator


class ReviewController extends Controller
{
    public function create(Request $request)
    {
        $rules = [ //membuat schema validator
            'user_id' => 'required|integer',
            'course_id' => 'required|integer',
            'rating' => 'required|integer|min:1|max:5',
            'note' => 'string'
        ];

        $data = $request->all(); //mengambil seluruh data dari body (req.body) in javascript

        $validator = Validator::make($data, $rules); //proses validasi menyesuaikan $data dari body dengan validasi $rules

        if ($validator->fails()) { //melakukan pengecekan apabila validasi gagal
            return response()->json([
                'status' => 'error', //akan mereturn status error
                'message' => $validator->errors()
            ], 400); //dengan status code 400
        }

        $courseId = $request->input('course_id');
        $course = Course::find($courseId);

        if(!$course) {
        return response()->json([
                'status' => 'error',
                'message' => "course not found"
            ], 404);
        }


        $userId = $request->input('user_id');
        $user = getUser($userId);

        if($user['status'] === 'error') {
            return response()->json([
                'status' => $user['status'],
                'message' => $user['message']
            ], $user['http_code']);
        }

        // membuat variabel pengecekan jika course id dan user id terduplicate
        $isExistMyCourse = Review::where('course_id', '=', $courseId)
                                    ->where('user_id', '=', $userId)
                                    ->exists();

        // jika user melakukan reovew yg sama 2x maka return error
        if($isExistMyCourse) {
            return response()->json([
            'status' => 'error',
            'message' => 'review already taken'
            ], 409);
        }

            // jika semua conditional sudah terlewati maka simpan ke dlm database
            $review = Review::create($data);
            return response()->json([
                'status' => 'success',
                'message' => $review
            ]);
    }
    public function update(Request $request, $id)
    {
        $rules = [
            'rating' => 'integer|min:1|max:5',
            'note' => 'string'
        ];

        $data = $request->except('user_id', 'course_id'); //memanggil seluruh data dari body kecuali user_id dan course_id

        $validator = Validator::make($data, $rules);

        if($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator.errors()
            ], 400);
        }

        $review = Review::find($id);
        if(!$review) {
            return response()->json([
                'status' => 'error',
                'message' => 'review not found'
            ], 404);
        }

        $review->fill($data);

        $review->save();

        return response()->json([
            'status' => 'success',
            'data' => $review
        ]);
    }
    public function destroy($id)
    {
        $review = Review::find($id);
        if(!$review) {
            return response()->json([
                'status' => 'error',
                'message' => 'review not found'
            ], 404);
        }
        
        $review->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'review deleted'
        ]);
    }
}
