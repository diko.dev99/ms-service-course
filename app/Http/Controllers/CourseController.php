<?php

namespace App\Http\Controllers;

use App\Course;  //calling model Course
use App\Mentor; //calling model Mentor
use App\Review;
use App\Chapter;
use App\MyCourse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class CourseController extends Controller
{
    // membuat function get data
    public function index(Request $request)
    {
        $courses = Course::query(); //menngambil seluruh data dari course dan membuat sebuah pagination

        $q = $request->query('q'); //mengambil query params q
        $status = $request->query('status'); //mengambil query params status

        $courses->when($q, function($query) use ($q) { //membuat filter dengan query q
            return $query->whereRaw("name LIKE '%".strtolower($q)."%'"); //merubah format ke huruf kecil
        });

        $courses->when($status, function($query) use ($status) { //membuat filter demgam query status
            return $query->where('status', '=', $status);
        });

        return response()->json([
            'status' => 'success',
            'data' => $courses->paginate(10) //set pagination 10/page
        ]);
    }
    // membuat function show untuk menampilkan detail course
    public function show($id)
    {
        // $course = Course::with('chapters', 'images', 'mentors')->find($id);
       $course = Course::with('chapters.lessons') //mengambil model chapters yg mengarah ke model lessons
                        ->with('images')
                        ->with('mentors')
                        ->find($id);
        
        if(!$course){
            return response()->json([
                'status' => 'error',
                'message' => 'course not found'
            ], 404);
        }

        $reviews = Review::where('course_id', '=', $id)->get()->toArray(); //resultnnya langsung diubah ke array biasa
        
        if(count($reviews) > 0) { //dijalankan hanya ketika data reviews tidak kosong
            // cek dokumentation php array column
            $userIds = array_column($reviews, 'user_id');
            $users = getUserByIds($userIds);
            
            if($users['status'] === 'error') { //jika service user down maka return []
                $reviews = [];
            } else {
                foreach($reviews as $key => $reviews) {
                    // cek documentation php array_search
                    $userIndex = array_search($reviews['user_id'], array_column($users['data'], 'id'));
                    $reviews[$key]['users'] = $users['data'][$userIndex];
                };
            }
        }
        
        $totalStudent = MyCourse::where('course_id', '=', $id)->count(); //menjumlahkan
        $totalVideos = Chapter::where('course_id', '=', $id)->withCount('lessons')->get()->toArray();
        $finalTotalVideos = array_sum(array_column($totalVideos, 'lessons_count'));


        $course['reviews'] = $reviews;
        $course['total_videos'] = $finalTotalVideos;        
        $course['total_student'] = $totalStudent;

        return response()->json([
            'status' => 'success',
            'data' => $course
        ]);
    }
    // membuat function create courses
    public function create(Request $request)
    {
        $rules = [ //created schema validasi
            'name' => 'required|string',
            'certificate' => 'required|boolean',
            'thumbanail' => 'string|url',
            'type' => 'required|in:free,premium', //error if content not free or premium 
            'status' => 'required|in:draft,published',
            'price' => 'integer',
            'level' => 'required|in:all-level,beginner,intermediate,advance',
            'mentor_id' => 'required|integer',
            'description' => 'string' 
        ];

        $data = $request->all(); //get all data from body

        $validator = Validator::make($data, $rules); //create validation

        if($validator->fails()) { //checking validation if any rules error
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $mentorId = $request->input('mentor_id'); //get data
        $mentor = Mentor::find($mentorId); //calling model Mentor

        if(!$mentor) { //if mentor not found return error
            return response()->json([
                'status' => 'error',
                'message' => 'mentor not found'
            ], 404);
        }

        //if data mentor is found
        $course = Course::create($data); //create to database
        return response()->json([
            'status' => 'success',
            'data' => $course
        ]);
    }
    // membuat function update courses
    public function update(Request $request, $id)
    {
        $rules = [ //created schema validasi
            'name' => 'string',
            'certificate' => 'boolean',
            'thumbanail' => 'string|url',
            'type' => 'in:free,premium', //error if content not free or premium 
            'status' => 'in:draft,published',
            'price' => 'integer',
            'level' => 'in:all-level,beginner,intermediate,advance',
            'mentor_id' => 'integer',
            'description' => 'string' 
        ];

        $data = $request->all(); //get all data from body

        $validator = Validator::make($data, $rules); //create validation

        if($validator->fails()) { //checking validation if any rules error
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $course = Course::find($id); //cek apakah id yg dimaksud ada table course
        if(!$course) { //jika tidak ada maka return error
            return response()->json([
                'status' => 'error',
                'message' => 'course not found'
            ], 404);
        }

        $mentorId = $request->input('mentor_id'); //cek mentor id jika dikirim dari sisi frontend
        if ($mentorId) {
            $mentor = Mentor::find($mentorId);
            if(!$mentor) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'mentor not found'

                ], 404);
            }
        }
        $course->fill($data);
            $course->save();
            return response()->json([
                'status' => 'success',
                'data' => $course
            ]);
    }
    // menhapus data course 
    public function destroy($id)
    {
        $course = Course::find($id); //mengecek data course apakah ada atau tidak
        if(!$course) { //jika course tidak ditemukan maka return error
            return response()->json([
                'status' => 'error',
                'message' => 'course not found'
            ], 404);
        }
        // jika tidak ada error maka masuk ketahap selanjutnya yaitu mendelete course berdasarkan id
        $course->delete();
        // mereturn response agar bisa digunakan
        return response()->json([
            'status' => 'success',
            'message' => 'courses deleted'
        ]);
    }
}
