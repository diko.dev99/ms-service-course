<?php

namespace App\Http\Controllers;

use App\Mentor; //memanggil model Mentor dari folder App
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; //mengimport validator

class MentorController extends Controller
{
    // get all data mentor in database
    public function index(Request $request)
    {
        $mentor = Mentor::all(); //method for get data from database
        return response()->json([ //handle success and return to json
            'status' => 'success',
            'data' => $mentor
        ]);
    }
    // get detail data mentor with id.params
    public function show($id)
    {
        $mentor = Mentor::find($id);
        if(!$mentor) {
            return response()->json([
                'status' => 'error',
                'message' => 'mentor not found'
            ], 404);
        }

        return response()->json([
            'status' => 'success',
            'data' => $mentor
        ]);
    }
    // method for create data mentor
    public function create(Request $request)
    {
        $rules = [ //membuat schema validator
            'name' => 'required|string',
            'profile' => 'required|url',
            'profession' => 'required|string',
            'email' => 'required|email'
        ];

        $data = $request->all(); //mengambil seluruh data dari body (req.body) in javascript

        $validator = Validator::make($data, $rules); //proses validasi menyesuaikan $data dari body dengan validasi $rules

        if ($validator->fails()) { //melakukan pengecekan apabila validasi gagal
            return response()->json([
                'status' => 'error', //akan mereturn status error
                'message' => $validator->errors()
            ], 400); //dengan status code 400
        }

        $mentor = Mentor::create($data); // apabila data tidak ada yg error setelah melewati validasi maka akan di create ke database

        // ketika data mentor telah ter create maka akan direturn ke sisi frontend dengan mengirimkan json
        return response()->json([
            'status' => 'success',
            'data' => $mentor //dengan data yang berisi variable yg sudah mencreated ke database
        ]);
    }
    // methods for update data mentor
    public function update(Request $request, $id)
    {
        $rules = [ //membuat schema validator
            'name' => 'string',
            'profile' => 'url',
            'profession' => 'string',
            'email' => 'email'
        ];

        $data = $request->all(); //mengambil seluruh data dari body (req.body) in javascript

        $validator = Validator::make($data , $rules); //proses validasi menyesuaikan $data dari body dengan validasi $rules

        if ($validator->fails()) { //melakukan pengecekan apabila validasi gagal
            return response()->json([
                'status' => 'error', //akan mereturn status error
                'message' => $validator->errors()
            ], 400); //dengan status code 400
        }

        $mentor = Mentor::find($id);

        if(!$mentor) {
            return response()->json([
                'status' => 'error',
                'message' => 'mentor not found'
            ], 404);
        }

        $mentor->fill($data); //mengupdate data dari body ke database

        $mentor->save();
        return response()->json([
            'status' => 'success',
            'data' => $mentor
        ]);
    }
    // methods for delete data mentor from database
    public function destroy($id)
    {
        $mentor = Mentor::find($id); //find data mentor in database with ID
        if(!$mentor) { //conditional if data mentor not found
            return response()->json([
                'status' => 'error',
                'message' => 'Mentor not found!' 
            ], 404);
        }

        // if data mentor founding, else delete data mentor with id
        $mentor->delete();
        return response()->json([ //reponse to json if success delete data mentor
            'status' => 'success',
            'message' => 'Mentor Deleted'
        ]);
    }
}
