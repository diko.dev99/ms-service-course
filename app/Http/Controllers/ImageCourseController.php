<?php

namespace App\Http\Controllers;

use App\ImageCourse; //calling model Mentor
use App\Course;  //calling model Course
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ImageCourseController extends Controller
{
    // membuat method create image-course
    public function create(Request $request)
    {
        $rules = [ //created schema validasi
            'image' => 'required|url',
            'course_id' => 'required|integer',
        ];

        $data = $request->all(); //get all data from body

        $validator = Validator::make($data, $rules); //create validation

        if($validator->fails()) { //checking validation if any rules error
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $courseId = $request->input('course_id');
        $course = Course::find($courseId);

        if(!$course) { //if course not found return error
            return response()->json([
                'status' => 'error',
                'message' => 'mentor not found'
            ], 404);
        }

        $imageCourse = ImageCourse::create($data);
        return response()->json([
            'status' => 'success',
            'data' => $imageCourse
        ]);
    }
    // membuat method menhapus data imageCourse 
    public function destroy($id)
    {
        $imageCourse = ImageCourse::find($id); //mengecek data course apakah ada atau tidak
        if(!$imageCourse) { //jika course tidak ditemukan maka return error
            return response()->json([
                'status' => 'error',
                'message' => 'image-courses not found'
            ], 404);
        }
        // jika tidak ada error maka masuk ketahap selanjutnya yaitu mendelete course berdasarkan id
        $imageCourse->delete();
        // mereturn response agar bisa digunakan
        return response()->json([
            'status' => 'success',
            'message' => 'image-courses deleted'
        ]);
    }
}
