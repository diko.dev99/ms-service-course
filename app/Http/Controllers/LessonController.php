<?php

namespace App\Http\Controllers;

use App\Lesson; //import from model Lesson
use App\Chapter; //import from model ChAPTER
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LessonController extends Controller
{
    // membuat method get data data dengan query id
    public function index(Request $request)
    {
        $lessons = Lesson::query(); //menngambil seluruh data dari lesson

        $chapterId = $request->query('chapter_id'); //mengambil query params chapter_id

        $lessons->when($chapterId, function($query) use ($chapterId) { //membuat filter demgan query chapter_id
            return $query->where('chapter_id', '=', $chapterId);
        });

        return response()->json([
            'status' => 'success',
            'data' => $lessons->get() //dont forget method get() if u make query params
        ]);
    }
    // membuat method get detail data chapter
    public function show($id)
    {
        $lessons = Lesson::find($id);
        if(!$lessons) {
            return response()->json([
                'status' => 'error',
                'message' => 'lessons not found'
            ], 404);
        }

        return response()->json([
            'status' => 'success',
            'data' => $lessons
        ]);
    }
    // membuat method create data lesson
    public function create(Request $request)
    {
        $rules = [ //create schema validation
            'name' => 'required|string',
            'video' => 'required|string',
            'chapter_id' => 'required|integer',
        ];

        $data = $request->all();

        $validator = Validator::make($data, $rules);
        if($validator->fails()){ // cek apabila validator gagal
            return response()->json([
                'status' => 'error',
                'message' =>  $validator->errors()
            ], 400); //bad request
        }

        $chapterId = $request->input('chapter_id');
        $chapter = Chapter::find($chapterId);
        if(!$chapter) {
            return response()->json([
                'status' => 'error',
                'message' => 'chapter not found'
            ], 404);
        }

        $lesson = Lesson::create($data);
        return response()->json([
            'status' => 'success',
            'data' => $lesson
        ]);
    }
    // membuat method update data lesson
    public function update(Request $request, $id)
    {
        $rules = [ //create schema validation
            'name' => 'string',
            'video' => 'string',
            'chapter_id' => 'integer',
        ];

        $data = $request->all();

        $validator = Validator::make($data, $rules);
        if($validator->fails()){ // cek apabila validator gagal
            return response()->json([
                'status' => 'error',
                'message' =>  $validator->errors()
            ], 400); //bad request
        }

        $lesson = Lesson::find($id); //mengambil data dari DB table Lesson dengan id yg dikirmkan dari depan
        if(!$lesson) {
            return response()->json([
                'status' => 'error',
                'message' => 'lesson not found'
            ], 404);
        }

        $chapterId = $request->input('chapter_id');
        if($chapterId) {
            $chapter = Chapter::find($chapterId);
            if(!$chapter) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'chapter not found'
                ], 404);
            }
        }

        $lesson->fill($data);
        $lesson->save();

        return response()->json([
            'status' => 'success',
            'data' => $lesson
        ]);
    }
    // membuat method delete data lesson
    public function destroy($id)
    {
        $lesson = Lesson::find($id);
        if(!$lesson) {
            return response()->json([
                'status' => 'error',
                'message' => 'lesson not found'
            ], 404);
        }

        $lesson->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'chapter deleted'
        ]);
    }
}
