<?php

namespace App\Http\Controllers;

use App\Course; //calling model course
use App\MyCourse;  //calling model mycourse
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MyCourseController extends Controller
{
    public function index(Request $request)
    {
        //noted MyCourse::query()->with('course'); from Model MyCourse and calling method course
        $myCourse = MyCourse::query()->with('course');

        $userId = $request->query('user_id');
        $myCourse->when($userId, function($query) use ($userId) {
            return $query->where('user_id', '=', $userId);
        });

        return response()->json([
            'status' => 'success',
            'data' => $myCourse->get()
        ]); 
    }
    // membuat method create
    public function create(Request $request) {
        $rules = [ //created schema validasi
            'course_id' => 'required|integer',
            'user_id' => 'required|integer',
        ];

        $data = $request->all();

        $validator = Validator::make($data, $rules); //create validation

        if($validator->fails()) { //checking validation if any rules error
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $courseId = $request->input('course_id');
        $course = Course::find($courseId);

        if(!$course) {
            return response()->json([
                'status' => 'error',
                'message' => 'course not found'
            ], 404);
        }

        $userId = $request->input('user_id');
        $user = getUser($userId);

        if($user['status'] === 'error') {
            return response()->json([
                'status' => $user['status'],
                'message' => $user['message']
            ], $user['http_code']);
        }

        // membuat variabel pengecekan jika course id dan user id terduplicate
        $isExistMyCourse = MyCourse::where('course_id', '=', $courseId)
                                    ->where('user_id', '=', $userId)
                                    ->exists();
        // jika user mengambil course yg sama 2x maka return error
        if($isExistMyCourse) {
            return response()->json([
                'status' => 'error',
                'message' => 'user already taken'
            ], 409);
        }

        // jika semua conditional sudah terlewati maka simpan ke dlm database
        $myCourse = MyCourse::create($data);

        return response()->json([
            'status' => 'success',
            'message' => $myCourse
        ]);
    }
}
