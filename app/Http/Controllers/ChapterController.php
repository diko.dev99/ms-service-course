<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chapter;
use App\Course;
use Illuminate\Support\Facades\Validator;

class ChapterController extends Controller
{
    // membuat method get data chapter
    public function index(Request $request)
    {
        $chapters = Chapter::query(); //mengambil semua list chapter
        $courseId = $request->query('course_id'); //query course_id

        $chapters->when($courseId, function($query) use ($courseId) { //membuat filter data berdasarkan query course_id
            return $query->where('course_id', '=', $courseId);
        });

        return response()->json([
            'status' => 'success',
            'data' => $chapters->get() //method get() untuk mengambil data terbaru sesuai filter
        ]);
    }
    // membuat method get detail data chapter
    public function show($id)
    {
        $chapter = Chapter::find($id);
        if(!$chapter) {
            return response()->json([
                'status' => 'error',
                'message' => 'chapter not found'
            ], 404);
        }

        return response()->json([
            'status' => 'success',
            'data' => $chapter
        ]);
    }
    // membuat method create chapter
    public function create(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'course_id' => 'required|integer'
        ];

        $data = $request->all();

        $validator = Validator::make($data, $rules);

        if($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $courseId = $request->input('course_id');
        $course = Course::find($courseId);

        if(!$course) {
            return response()->json([
                'status' => 'error',
                'message' => 'course not found'
            ], 404);
        }

        $chapter = Chapter::create($data);
        return response()->json([
            'status' => 'success',
            'message' => $chapter
        ]);
    }
    // membuat method update chapter
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'string',
            'course_id' => 'integer'
        ];

        $data = $request->all();

        $validator = Validator::make($data, $rules);

        if($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'message' => $validator->errors()
            ], 400);
        }

        $chapter = Chapter::find($id);
        if(!$chapter) {
            return response()->json([
                'status' => 'error',
                'chapter' => 'chapter not found'
            ], 404);
        }

        $courseId = $request->input('course_id'); //mengecek apabila course id dikirmkan dari frontend
        if($courseId) {
            $course =  Course::find($courseId);
            if(!$course) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'course not found'
                ], 404);
            }
        }

        $chapter->fill($data);
        $chapter->save();

        return response()->json([
            'status' => 'success',
            'data' => $chapter
        ]);
    }
    // membuat method delete data chapter
    public function destroy($id)
    {
        $chapter = Chapter::find($id);
        if(!$chapter) {
            return response()->json([
                'status' => 'error',
                'message' => 'chapter not found'
            ], 404);
        }

        $chapter->delete();
        return response()->json([
            'status' => 'success',
            'message' => 'chapter deleted'
        ]);
    }
}
