<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mentor extends Model
{
    protected $table = 'mentors'; //mengarahkan ke table mentors

    protected $casts = [ //merubah format waktu pada create_at dan update_at
        'created_at' => 'datetime:Y-m-d H:m:s',
        'updated_at' => 'datetime:Y-m-d H:m:s',
    ];

    protected $fillable = [ //mendeskripsikan kolom apa saja yg boleh diisi
        'name', 'profile', 'email', 'profession'
    ];
}
