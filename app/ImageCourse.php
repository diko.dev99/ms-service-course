<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageCourse extends Model
{
    protected $table = 'image_courses'; //mengarahkan ke table image_courses

    protected $fillable = [ //mendeskripsikan kolom apa saja yg boleh diisi
        'course_id', 'image'
    ];

    // variabel untuk merubah format tanggal created_at dan updated_at
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:m:s',
        'updated_at' => 'datetime:Y-m-d H:m:s'
    ];
}
