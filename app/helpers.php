<?php

// check documention laravel guzzle
## https://laravel.com/docs/8.x/http-client

use Illuminate\Support\Facades\Http;

// helper for communication service-user and service course
## when function getUser used ?
## if you wanna get user by id

function getUser($userId) {

   $url = env('SERVICE_USER_URL').'users/'.$userId; // calling variabel .env

   try {
      
      $response = Http::timeout(10)->get($url);
      $data = $response->json();
      $data['http_code'] = $response->getStatusCode(); //inject data http code      
      return $data;

   } catch (\Throwable $th) {

      // return if server service user down
      return [
         'status' => 'error',
         'http_code' => 500,
         'message' => 'service user unavailable'
      ];
   }

}


// ($userId = []) set param userId with default []
## function getUserByIds, is if you want get user 1 and user 2 or user 4 or 5 are you get user 1-5
function getUserByIds($userId = []) {

   $url = env('SERVICE_USER_URL').'users/'; //calling variabel .env

   try {
      
      if(count($userId) === 0) {
         return [
            'stataus' => 'success',
            'http_code' => 200,
            'data' => []
         ];
      }
      
      $response = Http::timeout(10)->get($url, ['user_ids[]' => $userId]); //add query params user_ids[]
      $data = $response->json();
      $data['http_code'] = $response->getStatusCode(); //inject data http code      
      return $data;

   } catch (\Throwable $th) {
      
      // return if service user down
      return [
         'status' => 'error',
         'http_code' => 500,
         'message' => 'service user unavailable'
      ];

   }

} 