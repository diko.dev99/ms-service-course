<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'reviews'; //mengarahkan ke table reviews

    protected $fillable = [ //mendeskripsikan kolom apa saja yg boleh diisi
        'user_id', 'course_id', 'rating', 'note'
    ];

    // variabel untuk merubah format tanggal created_at dan updated_at
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:m:s',
        'updated_at' => 'datetime:Y-m-d H:m:s'
    ];
    
    // method digunakan mengambil data courses dari dari model Review
    public function course()
    {
        return $this->belongsTo('App\Course');
    }

}
