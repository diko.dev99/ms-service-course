<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $table = 'chapters'; //mengarahkan ke table chapters

    protected $fillable = [ // mendeskripsikan kolom apa saja yang harus diiisi
        'name', 'course_id'
    ];

    // variabel untuk merubah format tanggal created_at dan updated_at
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:m:s',
        'updated_at' => 'datetime:Y-m-d H:m:s'
    ];

    // method untuk mendapatkan model lesson, dengan foreign Key
    public function lessons()
    {
        return $this->hasMany('App\Lesson')->orderBy('id', 'ASC');
    }

}
